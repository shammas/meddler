<?php
/**
 * Index.php
 * Date: 05/03/19
 * Time: 11:30 AM
 */

class Index extends CI_Controller { 
	 
        protected $header = 'templates/header';
        protected $footer = 'templates/footer';

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Client_model', 'client');
            $this->load->model('Product_model', 'product');
            // $this->load->model('Product_file_model', 'product_file');
        }

        protected $current = '';

        public function index()
        {
            $data['testimonials'] = $this->testimonial->limit(3)->order_by('id','desc')->get_all();
            $data['clients'] = $this->client->order_by('id','desc')->get_all();
           
            $this->current = 'index';
            $this->load->view($this->header, ['current' => $this->current]);
            $this->load->view('index',$data);
            $this->load->view($this->footer);
        }

        public function about()
        {
            $this->current = 'about';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('about');
            $this->load->view($this->footer);
        }

        public function contact() 
        {
            $this->current = 'contact';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('contact');
            $this->load->view($this->footer);
        }

        public function products()
        {
            $data['products'] = $this->product->with_files()->get_all();
            $this->current = 'products';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('products',$data);
            $this->load->view($this->footer);
        }

        // public function product_single()
        // {
        //     $this->current = 'product-single';
        //     $this->load->view($this->header, ['current' => $this->current]);

        //     $this->load->view('product-single');
        //     $this->load->view($this->footer);
        // }

        public function services()
        {
            $this->current = 'services';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('services');
            $this->load->view($this->footer);
        }

        public function test()
        {
            $this->load->view('test');
        }

        public function testimonial()
        {
            $data['testimonials'] = $this->testimonial->order_by('id','desc')->get_all();
            
            $this->current = 'testimonial';
            $this->load->view($this->header,['current' => $this->current]);

            $this->load->view('testimonial',$data);
            $this->load->view($this->footer);
        }
}