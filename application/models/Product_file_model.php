<?php
/**
 * Product_file_model.php
 * Date: 05/03/19
 * Time: 12:41 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Product_file_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}