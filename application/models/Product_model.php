<?php
/**
 * Product_model.php
 * Date: 05/03/19
 * Time: 12:50 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Product_model extends MY_Model
{

    function __construct()
    {
        $this->has_many['files'] = array(
            'foreign_model' => 'product_file_model',
            'foreign_table' => 'product_files',
            'foreign_key' => 'product_id',
            'local_key' => 'id'
        ); 
         
        parent::__construct();
        $this->timestamps = TRUE;
    }
    
}
            