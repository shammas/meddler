
  <header class="page-title pt-dark pt-plax-md-dark" data-stellar-background-ratio="0.4">
    <div class="bg-overlay">
      <div class="container">
        <div class="row">

          <div class="col-sm-6">
            <h1>Contact Us</h1>
            <span class="subheading">Uniform Makes Brotherhood</span>
          </div>
          <ol class="col-sm-6 text-right breadcrumb">
            <li><a href="index">Home</a></li>
            <li class="active">Contact Us</li>
          </ol>

        </div>
      </div>
    </div>
    </header>

  <section id="contact" class="section contact-2">
    <header class="sec-heading">
      <h2>Say Hello</h2>
      <span class="subheading">We love to discuss your idea</span>
    </header>
    <div class="footer-litle ws-m">
      <div class="">
        <address>
          <ul>
            <li>
              <span class="linea-basic-map adr-icon"></span>
              <div class="adr-group">
                <span class="adr-heading">Address</span>
                <span class="adr-info">9/14A, 6/573 E Chirangal Building,<br/>Pookattupady Road Mundampalam, <br/>Thrikkakara P.O, Ernakulam - 682 021</span>
              </div>
            </li>
            <li>
              <span class="linea-basic-paperplane adr-icon"></span>
              <div class="adr-group">
                <span class="adr-heading">Email</span>
                <span class="adr-info"><a href="mailto: meddlermakers@gmail.com" style="color: #000;">meddlermakers@gmail.com</a></span>
              </div>
            </li>
            <li>
              <span class="linea-basic-smartphone adr-icon"></span>
              <div class="adr-group">
                <span class="adr-heading">Phone</span>
                <span class="adr-info"><a href="tel:+91 7594948001" style="color: #000;">+91 75 9494 8001</a><br/> <a href="tel:+91 7594948002" style="color: #000;">+91 75 9494 8002</a><br/> <a href="tel:+91 7594948003" style="color: #000;">+91 75 9494 8003</a></span>
              </div>
            </li>
          </ul>
        </address>
      </div>
    </div>
    <div class="container" style="margin-bottom: 50px;">
      <div class="row">
        <form action="../assets/contact-form/contact-form.php" method="POST" id="contact-form-1"
          class="form-ajax">
          <div class="col-md-offset-2 col-md-4 wow fadeInUp" data-wow-duration="1s">
            <div class="form-group">
              <input type="text" name="name" id="name-contact-1" class="form-control validate-locally"
                placeholder="Enter your name">
              <label for="name-contact-1">Name</label>
              <span class="pull-right alert-error"></span>
            </div>
            <div class="form-group">
              <input type="email" name="email" id="email-contact-1" class="form-control validate-locally"
                placeholder="Enter your email">
              <label for="email-contact-1">Email</label>
              <span class="pull-right alert-error"></span>
            </div>
          </div>
          <div class="col-md-4 wow fadeInUp" data-wow-duration="1s">
            <div class="form-group">
              <textarea name="message" id="message-contact-1" class="form-control" rows="5"
                placeholder="Your Message"></textarea>
              <label for="message-contact-1">Message</label>
            </div>
            <div>
              <input type="submit" class="btn pull-right" value="Send Message">
            </div>
            <div class="ajax-message col-md-12 no-gap"></div>
          </div>
        </form>
      </div>
    </div>
    </section> 
