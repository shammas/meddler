
  <header class="page-title pt-dark pt-plax-md-dark" data-stellar-background-ratio="0.4" style="background: url(../img/Products-banner.jpg);">
    <div class="bg-overlay">
      <div class="container">
        <div class="row">

          <div class="col-sm-6">
            <h1>Our Products</h1>
            <span class="subheading">Uniform Makes Brotherhood</span>
          </div>
          <ol class="col-sm-6 text-right breadcrumb">
            <li><a href="index">Home</a></li>
            <li class="active">Our Products</li>
          </ol>

        </div>
      </div>
    </div>
    </header>

    <section>
        <div class="container section-shop">
            <div class="row mb-30">
            <?php
            if (isset($products) and $products != false) {
            foreach($products as $product) { 
                ?>
                <!-- Single Product -->
                <div class="col-xs-12 col-sm-6 col-lg-3">
                  <div class="shop-product-card">
                    <div class="product-image-wrapper">
                      <div class="shop-p-slider">
                        <?php 
                        if (isset($product->files) and $product->files != false) {
                            foreach ($product->files as  $file) {  
                            ?> 
                            <a href="product-single"><img src="<?php echo $file->url . $file->file_name;?>" alt="<?php echo $product->name; ?>"></a>
                            <?php 
                            }
                        }
                        ?>
                      </div>
                    </div>
                    <div class="product-meta">
                      <a href="product-single">
                        <h4 class="product-name"><?php echo $product->name; ?></h4>
                      </a>
                    </div>
                  </div>
                </div>
                <!-- Single Product -->
                <?php 
                }   
            }   
            ?>
            </div>
        </div>
    </section>

 