
  <header class="page-title pt-dark pt-plax-md-dark" data-stellar-background-ratio="0.4">
    <div class="bg-overlay">
      <div class="container">
        <div class="row">

          <div class="col-sm-6">
            <h1>About Us</h1>
            <span class="subheading">UNIFORM MAKES BROTHERHOOD</span>
          </div>
          <ol class="col-sm-6 text-right breadcrumb">
            <li><a href="index">Home</a></li>
            <li class="active">About Us</li>
          </ol>

        </div>
      </div>
    </div>
  </header>
  <section id="about" class="container">
    <div class="row section">

      <header class="sec-heading">
        <h2>meddler uniform makers</h2>
        <span class="subheading">Uniform Makes Brotherhood</span>
      </header>

      <div class="col-md-offset-1 col-md-5">
        <blockquote>
          Meddler is high profile uniforms manufacturer and supplier in Kerala. We assure ourselves in being a quality maintainers . We set trends by giving you the latest clothes at factory direct prices.
        </blockquote>
      </div>
      <div class="col-md-5">
        <p>
          The segments we serve range from schools to Security Military to Corporate to Medical to Education and Food & Beverages and many more. Our ethical approach of business has gained us a remarkable space as being the unbeatable uniform supplier through word-of mouth publicity by our satisfied uniforms customers in Kerala. We have the well equipped facility to provide all kinds of uniforms in various segments of institutions.
        </p>
      </div>
    </div>
    <div class="row section">
      <div class="col-md-offset-1 col-md-5">
        <h3 style="border-left: 10px solid #ac7d49;padding-left: 20px;">Our VISION</h3>
        <p>
          To be a the leading and universal provider of quality uniform in terms of excellent service star prominent apparel manufacturer in terms of quality, service standards and 100% customer satisfaction.
        </p>
      </div>
      <div class="col-md-5">
        <h3 style="border-left: 10px solid #ac7d49;padding-left: 20px;">Our MISSION</h3>
        <p>
          To create a high brand with superior quality products, Qely delivery and best customer services at reasonable price.
        </p>
      </div>
    </div>
    <div class="row section">
      <div class="col-md-offset-1 col-md-5">
        <p>
          Designing requires a team that is creative, innovative and responsive to the needs of their clients. The designers understand the brand identity, corporate colors, and particular job roles. Sketches are made with a few design concepts. The client has the final word while choosing one. Color palettes, garments, styles, and logos are discussed with the client.
        </p>
        <p>
          Embroidery is sometimes, there is a requirement by clients to put embroidered logos on the uniforms. Since the uniforms are always needed in bulk quantities, it isn’t possible to make it manually or single-head machine. There are multi-head machines that can use multiple threads simultaneously to make desired emblems or logos in no time. Though the machines are operated manually, the speed is just incredible. High-precision logos and designs get printed on the stitched uniforms.
        </p>
      </div>
      <div class="col-md-5">
        <p>
          Once the design is finalized, expert cutters cut sample uniforms of different sizes as per the design specifications. First-cut stitches are examined by experts and modifications, or corrections are carried out. A few rounds of refinement in the uniform manufacturing process brings out the perfect uniform.
        </p>
        <p>
          Quality control is not possible to produce perfectly stitched uniforms without stringent quality control. There are people who take care of the quality and perfection of the material.
        </p>
        <p>
          Delays or partial submissions hamper your image in front of the employees. It also creates dissatisfaction or unnecessary gossips in the organization that hampers the productivity. Discuss the timelines in advance and give some buffer time to the vendor. However, the deliveries should not be delayed
        </p>
      </div>
    </div><!-- / .row -->
    
    <div class="row ws-m">
      <div class="col-md-12 text-center">
        <a href="contact" class="btn-text">Get in touch</a>
      </div>
    </div>
  </section>









