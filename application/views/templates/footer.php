<a href="tel:+91 75-9494-8001" class="fixed-request">Call : +91 75-9494-8001</a>
<footer class="footer-widgets">
		<div class="container">
			<div class="row section">
				<div class="col-md-4 col-sm-6 mb-sm-100">
					<div class="widget about-widget">
						<h5 class="header-widget">About Us</h5>
						<p>
							Meddler Uniform Makers is high profile uniforms manufacturer and supplier
							in Kerala. We assure ourselves in being a quality maintainers . We set
							trends by giving you the latest clothes at factory direct prices. The
							segments we serve range from schools to Security Military to
							Corporate to Medical to Education and Food & Beverages and many
							more. Our ethical approach of business has gained us a remarkable
							space as being the unbeatable uniform supplier through word-of
							mouth publicity by our satisfied uniforms customers in Kerala.
						</p>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 mb-sm-100">
					<div class="widget about-widget">
						<h5 class="header-widget">Quick Links</h5>
						<ul>
							<li><a href="<?php echo base_url();?>index">Home</a></li>
							<li><a href="<?php echo base_url();?>about">About Us</a></li>
							<li><a href="<?php echo base_url();?>services">We Focused on</a></li>
							<li><a href="<?php echo base_url();?>products">Our Products</a></li>
							<li><a href="<?php echo base_url();?>contact">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 mb-sm-100">
					<div class="widget about-widget">
						<h5 class="header-widget">We Focused On</h5>
						<div class="focus">
							<ul>
								<li>School Uniforms</li>
								<li>Hospital Uniforms</li>
								<li>Corporate Uniforms</li>
								<li>Promotional Products</li>
								<li>Work wear Uniforms</li>
								<li>Home Textile</li>
								<li>Security & Military Uniform</li>
								<li>Sports wear</li>
							</ul>
							<ul>
								<li>Food & Beverages Jacket Manufactures</li>
								<li>Hotel Uniforms</li>
								<li>Industrial Uniforms</li>
								<li>Coverall Chef Coat</li>
								<li>Doctors Coat Nursing Coat</li>
								<li>Advocate Coat T-Shirt,Badges</li>
								<li>Belt Bag Socks Cap Tie etc</li>
								<li>Casual wear</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 mb-sm-100">
					<div class="widget about-widget">
						<h5 class="header-widget">Contact Us</h5>
						<p>9/14A, 6/573 E,</p>
						<p>Chirangal Building,</p>
						<p>Mundampalam, Thrikkakara Post,</p>
						<p>Ernakulam - 682 021. INDIA</p>
						<p><a href="mailto: meddlermakers@gmail.com">meddlermakers@gmail.com</a></p>
						<p><i class="fa fa-phone-square mr5"></i><a href="tel:+91 75-9494-8001">+91 75-9494-8001</a>,</p>
						<p><i class="fa fa-phone-square mr5"></i><a href="tel:+91 75-9494-8002">+91 75-9494-8002</a>,</p>
						<p><i class="fa fa-phone-square mr5"></i><a href="tel:+91 75-9494-8003">+91 75-9494-8003</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<small>&copy; 2019 Meddler Makers | Designed by
							<a class="no-style-link" href="http://cloudbery.com/" target="_blank">
								<img src="<?php echo base_url();?>img/cloudbery.png" alt="Cloudbery Solutions">
							</a>
						</small>
					</div>
					<div class="col-sm-6">
						<small><a href="#page-top" class="pull-right to-the-top">To the top<i class="fa fa-angle-up"></i></a></small>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php echo base_url();?>js/jquery-2.1.4.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery.easing.js"></script>
	<script src="<?php echo base_url();?>js/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap-hover-dropdown.min.js"></script>
	<script src="<?php echo base_url();?>js/smoothscroll.js"></script>
	<script src="<?php echo base_url();?>js/jquery.localScroll.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery.scrollTo.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery.stellar.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery.parallax.js"></script>
	<script src="<?php echo base_url();?>js/slick.min.js"></script>
	<script src="<?php echo base_url();?>js/isotope.min.js"></script>
	<script src="<?php echo base_url();?>js/wow.min.js"></script>
	<!-- Definity JS -->
	<script src="<?php echo base_url();?>js/main.js"></script>
</body>

</html>
