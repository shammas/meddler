<!DOCTYPE html> 
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta name="author" content="Cloudbery Solutions">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:title" content="" />
  <meta property="og:site_name" content="" />
  <meta property="og:url" content="http://meddlermakers.com" />
  <meta property="og:description" content="" />
  <meta property="og:type" content="website" />
  <title>Meddler Uniform Makers | We Assure The Quality</title>
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <link href="<?php echo base_url();?>img/favicon.png" rel="icon">

  <!-- ========== Style sheets ========== -->
  <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/main.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/responsive.css">
  <script src="<?php echo base_url();?>js/modernizr-2.8.3.min.js"></script>
<!-- ========== Style sheets for index and products========== -->
  <link rel="stylesheet" href="<?php echo base_url();?>css/slick.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/animate.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.structure.min.css">
</head>

<body id="page-top">
  <div class="top-nav">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p><i class="fa fa-map-marker" aria-hidden="true"></i>Mundampalam, Thrikkakara P.O, Ernakulam</p>
        </div>
        <div class="col-md-6">
          <p class="contact-details">
            <span><i class="fa fa-envelope" aria-hidden="true"></i> meddlermakers@gmail.com</span>
            <span><i class="fa fa-phone-square" aria-hidden="true"></i> <a href="tel:+9175-9494-8001" style="color: #fff;">+91 75-9494-8001</a></span>
          </p>
        </div>
      </div>
    </div>
  </div>
  <nav class="navbar navbar-default navbar-fixed-top mega navbar-trans">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
          aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url();?>index"><img class="navbar-logo" src="<?php echo base_url();?>img/logo.png" alt="Meddler Uniform Makers"></a>
      </div>
      <!-- Navbar Links -->
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="<?php echo ($current == 'index' ? 'active' :'')?>"><a href="<?php echo base_url();?>index"> Home</a></li>
          <li class="<?php echo ($current == 'about' ? 'active' :'')?>"><a href="<?php echo base_url();?>about"> About Us </a></li>
          <li class="<?php echo ($current == 'services' ? 'active' :'')?>"><a href="<?php echo base_url();?>services"> Focused On </a></li>
          <li class="<?php echo ($current == 'testimonial' ? 'active' :'')?>"><a href="<?php echo base_url();?>testimonial"> clients says</a></li>
          <li class="<?php echo ($current == 'products' ? 'active' :'')?>"><a href="<?php echo base_url();?>products"> Products </a></li>
          <li class="<?php echo ($current == 'contact' ? 'active' :'')?>"><a href="<?php echo base_url();?>contact"> Contact Us </a></li>
        </ul>

      </div>
    </div>
  </nav>