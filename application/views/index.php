
	<div id="home" class="fw-slider-hero">
		<div class="fw-slider">
			<!-- Slide 1 -->
			<div class="fw-slider-item fw-slide-1">
				<div class="bg-overlay">
					<div class="hero-content-wrapper">
						<div class="hero-content">
							<h1 class="hero-lead wow fadeInUp" data-wow-duration="2s">We Assure The Quality</h1>
							<h4 class="h-alt hero-subheading wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">UNIFORM MAKES BROTHERHOOD</h4>
							<a href="services" class="btn btn-light wow fadeIn" data-wow-delay=".7s" data-wow-duration="2s">Learn More</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Slide 2 -->
			<div class="fw-slider-item fw-slide-2">
				<div class="bg-overlay">
					<div class="hero-content-wrapper">
						<div class="hero-content">
							<h1 class="hero-lead">Created<br>for creatives</h1>
							<h4 class="h-alt hero-subheading">UNIFORM MAKES BROTHERHOOD</h4>
							<a href="products" class="btn-light">Our products</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Slide 3 -->
			<div class="fw-slider-item fw-slide-3">
				<div class="bg-overlay">
					<div class="hero-content-wrapper">
						<div class="hero-content">
							<h4 class="h-alt hero-subheading wow fadeInUp">UNIFORM MAKES BROTHERHOOD</h4>
							<h1 class="hero-lead">Creative Agency</h1>
							<a href="contact" class="btn btn-light wow fadeInDown">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="services">
		<section class="container section ft-cards-2">
			<header class="sec-heading">
				<h2>What we do</h2>
				<span class="subheading">UNIFORM MAKES BROTHERHOOD</span>
			</header>

			<div class="row">
				<div class="col-md-6">
					<!-- Item 1 -->
					<div class="ft-card-item wow fadeInUp" data-wow-duration="1.2s">
						<img src="img/home-service-1.jpg" alt="Feature Image">
						<div class="ft-content">
							<h5>SCHOOL UNIFORM</h5>
							<p>High quality stitching a range of fabrics to choose from 100% cotton for summer uniforms & winter uniforms can be customized to the requirements.</p>
						</div>
					</div>
					<!-- Item 2 -->
					<div class="ft-card-item wow fadeInUp" data-wow-delay=".2s" data-wow-duration="1.2s">
						<img src="img/home-service-2.png" alt="Feature Image">
						<div class="ft-content">
							<h5>HOSPITAL UNIFORM</h5>
							<p>We Supply uniforms for HOSPITALS , DOCTORS,LAB COATS,NURSE UNIFORMS, SCRUB SUITS, PATIENT GOWNS,LABCOTS PHARMACY UNIFORM which are Comfortable,</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<!-- Item 3 -->
					<div class="ft-card-item wow fadeInUp" data-wow-duration="1.2s">
						<img src="img/home-service-3.jpg" alt="Feature Image">
						<div class="ft-content">
							<h5>SECURITY UNIFORM</h5>
							<p>We are known among the top most organizations of the industry for our exclusive range of Security Guard Shirts.</p>
						</div>
					</div>
					<!-- Item 4 -->
					<div class="ft-card-item wow fadeInUp" data-wow-delay=".2s" data-wow-duration="1.2s">
						<img src="img/home-service-4.jpg" alt="Feature Image">
						<div class="ft-content">
							<h5>CORPORATE UNIFORM</h5>
							<p>With talented Fashion designers and the latest apparel technology, we’re proud to be your source for corporate uniforms and merchandise.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<section class="container ft-steps-numbers">
		<div class="row section">

			<header class="sec-heading ws-s">
				<h2>Our work process</h2>
				<span class="subheading">UNIFORM MAKES BROTHERHOOD</span>
			</header>

			<!-- Step 1 -->
			<div class="col-lg-4 col-md-6 mb-sm-100 ft-item wow fadeIn" data-wow-duration="1s">
				<span class="ft-nbr">01</span>
				<h4>REQUIRMENTS and SAMPLING</h4>
				<p>The designers understand the brand identity, corporate colors, and particular job roles. Sketches are made with a few design concepts.</p>
			</div>

			<!-- Step 2 -->
			<div class="col-lg-4 col-md-6 mb-sm-100 ft-item wow fadeIn" data-wow-duration="1s"
			  data-wow-delay=".3s">
				<span class="ft-nbr">02</span>
				<h4>PRODUCTION and QUALITY</h4>
				<p>We produce perfectly stitched uniforms without stringent quality control. Who take care of the quality and perfection of the material.</p>
			</div>

			<!-- Step 3 -->
			<div class="col-lg-4 col-md-6 ft-item wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
				<span class="ft-nbr">03</span>
				<h4>Timely delivery</h4>
				<p>Discuss the timelines in advance and give some buffer time to the vendor. However, the deliveries should not be delayed.</p>
			</div>

		</div>

		<div class="row ws-m">
			<div class="text-center">
				<a href="#contact" class="btn">Get a free quote</a>
			</div>
		</div>

	</section>
	<section class="container testimonials-3col">
		<div class="row ws-m">
			<header class="sec-heading">
				<h2>What people say about us</h2>
				<span class="subheading">Read some of the testimonials from our amazing clients</span>
			</header>
			<?php
        	if (isset($testimonials) and $testimonials) {
	            foreach ($testimonials as $testimonial) {
	            ?>
				<div class="col-md-4 mb-sm-50">
					<div class="t-item wow fadeIn" data-wow-duration="1s">
						<img src="<?php echo $testimonial->url . $testimonial->file_name;?>" alt="<?php echo $testimonial->name;?>">
						<blockquote>
							<p> <?php echo $testimonial->description;?> </p>
							<footer>
								<cite>by <?php echo $testimonial->name;?><span><?php echo $testimonial->designation;?></span></cite>
							</footer>
						</blockquote>
						<span class="et-quote t-icon"></span>
					</div>
				</div>
				<?php 
				}
			}
			?>
		</div>
		<!-- Client Slider -->
		<div class="row">
			<ul class="t-clients clients-slider ws-m">
			<?php
            if (isset($clients) and $clients) {
                foreach ($clients as $client) {
                ?>
				<li><a href="#"><img src="<?php echo $client->url . $client->file_name;?>" alt="<?php echo $client->name;?>"></a></li>
				<?php 
				}
			}
			?>
			</ul>
		</div>
	</section>










	