
    <header class="page-title pt-dark pt-plax-md-dark" data-stellar-background-ratio="0.4" style="background: url(../img/Clients-banner.jpg);">
    <div class="bg-overlay">
      <div class="container">
        <div class="row">

          <div class="col-sm-6">
            <h1>Clients Says</h1>
            <span class="subheading">Uniform Makes Brotherhood</span>
          </div>
          <ol class="col-sm-6 text-right breadcrumb">
            <li><a href="index">Home</a></li>
            <li class="active">Clients Says</li>
          </ol>

        </div>
      </div>
    </div>
    </header>

    <section class="container testimonials-3col">
        <div class="row ws-m">
        <?php
        if (isset($testimonials) and $testimonials) {
            foreach ($testimonials as $testimonial) {
            ?>
            <div class="col-md-4 mb-sm-50 mb-30">
                <div class="t-item wow fadeIn" data-wow-duration="1s">
                  <img src="<?php echo $testimonial->url . $testimonial->file_name;?>" alt="<?php echo $testimonial->name;?>">
                  <blockquote>
                    <p> <?php echo $testimonial->description;?> </p>
                    <footer>
                      <cite>by <?php echo $testimonial->name;?><span><?php echo $testimonial->designation;?></span></cite>
                    </footer>
                  </blockquote>
                  <span class="et-quote t-icon"></span>
                </div>
            </div>
            <?php          
            }
        }
        ?>
        </div>
    </section>








  