
  <header class="page-title pt-dark pt-plax-md-dark" data-stellar-background-ratio="0.4" style="background: url(../img/services-banner.jpg);">
    <div class="bg-overlay">
      <div class="container">
        <div class="row">

          <div class="col-sm-6">
            <h1>we focused on</h1>
            <span class="subheading">Uniform Makes Brotherhood</span>
          </div>
          <ol class="col-sm-6 text-right breadcrumb">
            <li><a href="index">Home</a></li>
            <li class="active">we focused on</li>
          </ol>

        </div>
      </div>
    </div>
    </header>

  <div class="container-fluid ft-layout-50">
    <div class="row">
      <div class="ft-item">
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/School Uniform.jpg" alt="Features Image">
        </div>
        <div class="col-lg-6 ft-content-wrapper">
          <h3>School Uniform</h3>
          <p>
            MEDDLER UNIFORM MAKERS is one of the trusted name in Ernakulam city , Meddler  Uniform Makers are Manufacturers in Ernakulam & has provided with quality Children’s wear and School Uniforms, School Sportswear Tshirts Polo Tshirts and College uniforms Labcoats Workshop Uniforms at great prices. MEDDLER UNIFORM MAKERS strongly believes that appearance and presentation does matter! Meddler Uniforms are simply smarter. A Staff student spend an entire day and probably a quarter of one's life in uniforms, It is a product that shouldn't be compromised at any stage right from quality to appearance.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Hospital Uniform</h3>
          <p>
            Meddler Hospital Uniforms Ernakulam are Manufacturers & Supply uniforms for HOSPITALS , DOCTORS,LAB COATS,NURSE UNIFORMS, SCRUB SUITS, PATIENT GOWNS,LABCOTS PHARMACY UNIFORM which are Comfortable, Stylish and long lasting. We currently cater to the Healthcare and Medical Education sectors including Nursing Colleges. Our range of products allow the customer to choose colours consistent with their brand and co-ordinate styles across their departments. We make responsible service commitments and ensure that we deliver value with integrity.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/HOSPITAL UNIFORM.jpg" alt="Features Image">
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Corporate Uniform.png" alt="Features Image">
        </div>
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Corporate Uniform</h3>
          <p>
            Meddler Uniform Makers is a full service provider of Corporate apparel and promotional items. We are the pioneer brand in field of uniforms industry we oversee and ensure the process and the quality. Reinforce your brand image and positively impact your team’s enthusiasm and productivity by creating a unique uniform program that is on-trend and functional. We’ve created the most distinctive Corporate Uniform and innovative designs to fit your industry and budget.With talented Fashion designers and the latest apparel technology, we’re proud to be your source for corporate uniforms and merchandise.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Promotional Products</h3>
          <p>
            Promotional merchandise is products branded with a logo or slogan and distributed at little or no cost to promote a brand, corporate identity, or event. Such products, which are often informally called swag or schwag (mass nouns) or tchotchkes or freebies (count nouns) are used in marketing and sales. They are given away or sold at a loss to promote a company, corporate image, brand, or event. They are often distributed as handouts at trade shows, at conferences, on sales calls (that is, visits to companies that are purchasing or might purchase), and as bonus items in shipped orders. They are often used in guerrilla marketing campaigns.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Promotional Products.jpg" alt="Features Image">
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Home Textiles.jpg" alt="Features Image">
        </div>
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Home Textiles</h3>
          <p>
            Home textiles refer to the textiles used for home furnishing. It comprise of extensive range of functional as well as decorative items or products used mainly for the purpose of decorating    homes. Textile home furnishing fabrics or home textiles consist of both natural and man-made fabrics. At times, these textile fabrics are made strong and durable by blending them.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Security & Military Uniform</h3>
          <p>
            We own a rich industrial experience and expertise in offering Security & Military Uniforms. They have high quality fabric for the manufacturing, which give a desired fitting to the user. Our Uniform Dresses has distinctive appearance and smooth texture. They are finely finished in several processing and delivered using customized packaging. All the products are very competitively priced to match the customers' expectations. We are known among the top most organizations of the industry for our exclusive range of Security Guard Shirts.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Security & Military.jpg" alt="Features Image">
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Sportswear & Casual wear.jpg" alt="Features Image">
        </div>
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Sportswear & Casual wear</h3>
          <p>
            We offer a wide range of Sportswear in the market. Our products ranges are manufactured using the high grade of fabric. They are resistant to stain and flawlessly finished during the manufacturing process. We assure our clients about the quality of our Sportswear. We feel immense pleasure in offering our clients a wide range of Sportwear. We use skin-friendly fabric to develop our entire range of products. We assure our clients that our Sportswear will never cause irritation to the user and provide perfect fitting. They are perfectly stitched using high quality machines.
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Hotel Uniform</h3>
          <p>
            We feel proud to offer our customer base with a premium range of Chef Coat. The designs are attractive and are available in distinctive designing by which they look very attractive in appearance. The quality of our Chef Coat is widely appreciated and is offered at a very reasonable range. Moreover, we are known to offer them at very pocket friendly rates. We are counted among the top most organization of the industry due to our firm dedication in maintaining our qualitative goods without any compromise. Our wide ranges of Chef dress are easy to clean and easy to maintain. 
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Hotel.jpg" alt="Features Image">
        </div>
      </div>
      <div class="ft-item">
        <div class="col-lg-6 ft-img-wrapper">
          <img src="img/Industrial.jpg" alt="Features Image">
        </div>
        <div class="col-lg-6 ft-content-wrapper">
          <h3>Industrial Uniform </h3>
          <p>
            Each day at Meddler Uniform Makers are manufacturers of product range of Industrial workwear, protective coveralls, enhanced visibility workwear, corporate clothing, and other PPE are manufactured using cutting edge technology in production and using only the latest tried & tested high quality fabrics which are flame retardant, water repellent, oil repellent, and anti-static. We give special emphasis to Quality Control at each and every step in our production process. Raw materials of high quality fabric are sourced from the best in the industry and subjected to an insightfully engineered production line. 
          </p>
          <button class="btn-ghost">Call to Action</button>
        </div>
      </div>
    </div>
  </div>
 